# scan2rpdf
`scan2rpdf` is a shell script for unix-like operating systems that automates
scanning and creating _readable_ pdf files with OCR software.

## Requirements
This script uses the applications from the following packages:
  - sane
  - poppler
  - tesseract

The exact package names may vary on your distribution.


## Examples
```bash
scan2rpdf -h
```
This will show a help dialog explaing all available options.

```bash
scan2rpdf -x 70 -y 100
```
This will scan an area of 70mm x 100mm and write the output to **scan.pdf**.
The default size is 210mm x 297mm (DIN A4).

```bash
scan2rpdf -l eng -o english-page
```
This will scan an english A4 page and write the output to **english-page.pdf**.
The default language is **deu** :de:.

```bash
scan2rpdf -a -o multipage-document
```
This will fetch several pages from your ADF (Automated Document Feed) and
save the multipage pdf to **multipage-document.pdf**.


## Device Selection
Sometimes you may need to specify the scanning device.
In order to get a list of all available devices, do:
```bash
scanimage -L
```

Select the correct device with:
```bash
scan2rpdf -d <device-name>
```
